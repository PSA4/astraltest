﻿using Astral.Application.Interfaces;
using Astral.Application.Models.ConfigurationModels;
using Astral.Domain;
using Astral.Application;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.IO;
using System.Reflection;

namespace Astral.Api.Extensions
{
    /// <summary>
    /// Конфигурирование приложения.
    /// </summary>
    public static class ConfigurationStartUpExtension
    {
        /// <summary>
        /// Добавление сервисов
        /// </summary>
        public static void AddServices(this IServiceCollection services)
        {
            services.AddScoped<ISearchService, SearchService>();
            services.AddScoped<IVacancyStorage, VacancyStorage>();
            services.AddScoped<ISearchHttpClient, SearchHttpClient>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
        }

        /// <summary>
        /// Чтение конфигурации приложения в модель
        /// </summary>
        public static void LoadConfiguration(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<AppConfiguration>(configuration);
        }

        /// <summary>
        /// Добавление контекста БД
        /// </summary>
        public static void AddDatabaseServices(this IServiceCollection services, IConfiguration configuration)
        {
            var dbConnectionString = configuration["DbConnectionString"] ?? configuration.GetConnectionString("Default");

            services
               .AddDbContextPool<AstralContext>(options => options.UseInMemoryDatabase());
        }

        /// <summary>
        /// Добавление Swagger
        /// </summary>
        public static void AddSwagger(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "My API", Version = "v1" });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
        }
    }
}