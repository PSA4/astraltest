﻿using Astral.Application.Interfaces;
using Astral.Domain.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Astral.Api.Controllers
{
    [Route("api/")]
    [ApiController]
    public class VacancyController : ControllerBase
    {
        #region Поля

        /// <summary>
        /// Сервис по поиску вакансий
        /// </summary>
        private readonly ISearchService _searchService;

        #endregion Поля

        #region Конструктор

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="searchService">Сервис по поиску вакансий</param>
        public VacancyController(ISearchService searchService)
        {
            _searchService = searchService;
        }

        #endregion Конструктор

        #region Rest методы

        /// <summary>
        /// Возвращает список вакансий
        /// </summary>
        /// <param name="searchText">Поисковое слово</param>
        /// <returns>Список вакансий</returns>
        [HttpGet("vacancy")]
        public async Task<object> GetVacancies(string searchText)
        {
            var result = default(IEnumerable<Vacancy>);

            try
            {
                result = await _searchService.GetVacancies(searchText);
            }
            catch
            {
                BadRequest();
            }
            

            return result;
        }

        #endregion Rest методы
    }
}