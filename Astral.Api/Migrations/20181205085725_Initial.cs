﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Astral.Api.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Vacancy",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    VacancyId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    SalaryFrom = table.Column<decimal>(nullable: true),
                    SalaryTo = table.Column<decimal>(nullable: true),
                    Currency = table.Column<string>(nullable: true),
                    Employer = table.Column<string>(nullable: true),
                    Requirement = table.Column<string>(nullable: true),
                    Responsibility = table.Column<string>(nullable: true),
                    ResourceType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vacancy", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Vacancy");
        }
    }
}