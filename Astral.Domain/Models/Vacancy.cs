﻿using Astral.Domain.Enums;
using System;

namespace Astral.Domain.Models
{
    /// <summary>
    /// Вакансия
    /// </summary>
    public class Vacancy
    {
        /// <summary>
        /// Внутренний идентификатор вакансии
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Идентификатор внешней системы
        /// </summary>
        public int VacancyId { get; set; }

        /// <summary>
        /// Наименование вакансии
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Уровень ЗП от
        /// </summary>
        public decimal? SalaryFrom { get; set; }

        /// <summary>
        /// Уровень ЗП до
        /// </summary>
        public decimal? SalaryTo { get; set; }

        /// <summary>
        /// Валюта
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// Работадатель
        /// </summary>
        public string Employer { get; set; }

        /// <summary>
        /// Требование по вакансии
        /// </summary>
        public string Requirement { get; set; }

        /// <summary>
        /// Обязанности по вакансии
        /// </summary>
        public string Responsibility { get; set; }

        /// <summary>
        /// Откуда вакансия
        /// </summary>
        public ResourceType ResourceType { get; set; }
    }
}