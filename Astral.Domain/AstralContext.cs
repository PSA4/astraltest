﻿using Astral.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace Astral.Domain
{
    /// <summary>
    /// Контекст работы с БД
    /// </summary>
    public class AstralContext : DbContext
    {
        #region Таблицы

        /// <summary>
        /// Вакансии
        /// </summary>
        public DbSet<Vacancy> Vacancy { get; set; }

        #endregion Таблицы

        #region Конструктор

        public AstralContext(DbContextOptions<AstralContext> options)
            : base(options)
        {
        }

        #endregion Конструктор
    }
}