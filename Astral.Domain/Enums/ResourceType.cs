﻿namespace Astral.Domain.Enums
{
    /// <summary>
    /// Тип ресурса откуда вакансия
    /// </summary>
    public enum ResourceType
    {
        /// <summary>
        /// Создана локально в приложении
        /// </summary>
        local = 1,

        /// <summary>
        /// Из HH.ru
        /// </summary>
        HeadHunter = 2
    }
}