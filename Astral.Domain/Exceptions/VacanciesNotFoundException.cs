﻿using System;

namespace Astral.Domain.Exceptions
{
    /// <summary>
    /// Ошибка связанная с попыткой поиска вакансий на сторонних ресурсах
    /// </summary>
    public class VacanciesNotFoundException : Exception
    {
        /// <summary>
        /// Инициирует ошибку
        /// </summary>
        /// <param name="message"></param>
        public VacanciesNotFoundException(string message) : base(message)
        {
        }
    }
}