﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Astral.Application.Interfaces;
using Astral.Domain.Models;
using Astral.Domain;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Astral.Application.Repositories
{
    class VacancyRepository : IVacancy
    {
        #region Поля

        /// <summary>
        /// Контекст работы с БД
        /// </summary>
        private readonly AstralContext _context;

        #endregion Поля

        #region Конструктор

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="context">Контекст работы с БД</param>
        public VacancyRepository(AstralContext context)
        {
            _context = context;
        }

        #endregion Конструктор

        #region Методы

        /// <summary>
        /// Удалить вакансии
        /// </summary>
        /// <param name="vacancies">Вакансия(и)</param>
        public async Task Delete(Vacancy[] vacancies)
        {
            var crossing = _context.Vacancy.Where(p => vacancies.Select(x => x.VacancyId).Contains(p.VacancyId) && p.ResourceType == vacancies.FirstOrDefault().ResourceType);

            if (crossing != null)
            {
                _context.RemoveRange(crossing);
            }
        }

        /// <summary>
        /// Возвращает список вакансий
        /// </summary>
        /// <param name="searchText">Поисковое слово</param>
        /// <returns>Список вакансий</returns>
        public async Task<List<Vacancy>> GetBySearchText(string searchText)
        {
            var result = await _context.Vacancy.Where(p =>
                                                   (!String.IsNullOrEmpty(p.Name) && p.Name.ToLower().Contains(searchText)) ||
                                                   (!String.IsNullOrEmpty(p.Employer) && p.Employer.ToLower().Contains(searchText)) ||
                                                   (!String.IsNullOrEmpty(p.Requirement) && p.Requirement.ToLower().Contains(searchText)) ||
                                                   (!String.IsNullOrEmpty(p.Responsibility) && p.Responsibility.ToLower().Contains(searchText)))
                                                   .Take(50)
                                                   .ToListAsync();
            return result;
        }

        /// <summary>
        /// Добавить вакансии
        /// </summary>
        /// <param name="vacancies">Вакансия(и)</param>
        public async Task Insert(Vacancy[] vacancies)
        {
            if (vacancies == null)
                throw new NullReferenceException();

            await Delete(vacancies);
            await _context.AddRangeAsync(vacancies);
        }

        #endregion Методы
    }
}
