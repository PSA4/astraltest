﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Astral.Domain.Models;

namespace Astral.Application.Interfaces
{
    /// <summary>
    /// Интерфейс репозитория по вакансиям
    /// </summary>
    public interface IVacancy
    {
        /// <summary>
        /// Поиск вакансий по поисковому слову
        /// </summary>
        /// <param name="searchText">Текст к поиску</param>
        /// <returns>Список вакансий</returns>
        Task<List<Vacancy>> GetBySearchText(string searchText);

        /// <summary>
        /// Добавление списка вакансий
        /// </summary>
        /// <param name="vacancies">Список вакансий</param>
        Task Insert(Vacancy[] vacancies);

        /// <summary>
        /// Удаление списка вакансий
        /// </summary>
        /// <param name="vacancies">Список вакансий</param>
        Task Delete(Vacancy[] vacancies);
    }
}
