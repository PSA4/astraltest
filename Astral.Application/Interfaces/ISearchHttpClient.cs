﻿using Astral.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Astral.Application.Interfaces
{
    /// <summary>
    /// Интерфейс HTTP-клиента для поиска вакансий из стороннего ресурса
    /// </summary>
    public interface ISearchHttpClient
    {
        /// <summary>
        /// Поиск по вакансиям со стороннего ресурса
        /// </summary>
        /// <param name="text">Текст к поиску</param>
        /// <returns>Список вакансий</returns>
        Task<IEnumerable<Vacancy>> GetVacancyAsync(string text);
    }
}