﻿using Astral.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Astral.Application.Interfaces
{
    /// <summary>
    /// Интрефейс хранилища вакансий
    /// </summary>
    public interface IVacancyStorage
    {
        /// <summary>
        /// Сохранить вакансию в БД
        /// </summary>
        /// <param name="vacancies">Вакансия(и)</param>
        Task SaveVacancy(params Vacancy[] vacancies);

        /// <summary>
        /// Возвращает список вакансий согласно поисковому слову
        /// </summary>
        /// <param name="searchText">Поисковое слово</param>
        /// <returns>Список вакансий</returns>
        Task<List<Vacancy>> GetVacancies(string searchText);
    }
}