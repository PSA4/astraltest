﻿using System;
using System.Threading.Tasks;

namespace Astral.Application.Interfaces
{
    /// <summary>
    /// Интерфейс контекста работы с репозиториями
    /// </summary>
    public interface IUnitOfWork : IDisposable
    {
        /// <summary>
        /// Репозиторий по вакансиям
        /// </summary>
        IVacancy Vacancy { get; }

        /// <summary>
        /// Сохрание
        /// </summary>
        Task Save();
    }
}
