﻿using Astral.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Astral.Application.Interfaces
{
    /// <summary>
    /// Интерфейс сервиса по поиску вакансий
    /// </summary>
    public interface ISearchService
    {
        /// <summary>
        /// Возвращает список вакансий по поиковому слову
        /// </summary>
        /// <param name="text">Поисковое слово</param>
        /// <returns>Список вакансий</returns>
        Task<IEnumerable<Vacancy>> GetVacancies(string text);
    }
}