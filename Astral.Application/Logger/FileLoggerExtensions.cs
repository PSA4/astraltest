﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Logging;

namespace Astral.Application.Logger
{
    public static class FileLoggerExtensions
    {
        #region Методы

        /// <summary>
        /// Добавляет провайдер логгирования
        /// </summary>
        /// <param name="filePath">Путь к файлу</param>
        /// <returns>Провайдер логгирования</returns>
        public static ILoggerFactory AddFile(this ILoggerFactory factory, string filePath)
        {
            factory.AddProvider(new FileLoggerProvider(filePath));
            return factory;
        }

        #endregion Методы
    }
}
