﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Logging;

namespace Astral.Application.Logger
{
    public class FileLoggerProvider : ILoggerProvider
    {
        #region Поля

        /// <summary>
        /// Путь к файлу
        /// </summary>
        private string path;

        #endregion Поля

        #region Конструкторы

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="_path">Путь к файлу</param>
        public FileLoggerProvider(string _path)
        {
            path = _path;
        }

        #endregion Конструкторы

        #region Методы

        /// <summary>
        /// Создание объекта логгера.
        /// </summary>
        /// <param name="categoryName">Название категории для сообщений</param>
        /// <returns>Объект логгера</returns>
        public ILogger CreateLogger(string categoryName)
        {
            return new FileLogger(path);
        }

        /// <summary>
        /// Управляет освобождение ресурсов.
        /// </summary>
        public void Dispose()
        {
        }

        #endregion Методы
    }
}
