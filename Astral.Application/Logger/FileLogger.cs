﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Logging;

namespace Astral.Application.Logger
{
    public class FileLogger : ILogger
    {
        #region Поля

        /// <summary>
        /// Адрес текстового файла
        /// </summary>
        private string filePath;

        /// <summary>
        /// Блокировка заданного объекта
        /// </summary>
        private object _lock = new object();

        #endregion Поля

        #region Конструкторы

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="path">Адрес текстового файла</param>
        public FileLogger(string path)
        {
            filePath = path;
        }

        #endregion Конструкторы

        #region Методы

        /// <summary>
        /// Возвращает область видимости для логгера
        /// </summary>
        /// <param name="state">некоторый объект состояния</param>
        /// <returns>Область видимости</returns>
        public IDisposable BeginScope<TState>(TState state)
        {
            return null;
        }

        /// <summary>
        /// Возвращает значения, которые указывают, доступен ли логгер для использования
        /// </summary>
        /// <param name="logLevel">Уровень детализации текущего сообщения</param>
        /// <returns>Доступен ли логгер для использования</returns>
        public bool IsEnabled(LogLevel logLevel)
        {
            return true;
        }

        /// <summary>
        ///  Выполнение логгирования
        /// </summary>
        /// <param name="logLevel">Уровень детализации текущего сообщения</param>
        /// <param name="eventId">Идентификатор события</param>
        /// <param name="state">Некоторый объект состояния, который хранит сообщение</param>
        /// <param name="exception">Информация об исключении</param>
        /// <param name="formatter">Функция форматирвания</param>
        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            if(formatter != null)
            {
                lock (_lock)
                {
                    File.AppendAllText(filePath, formatter(state, exception) + Environment.NewLine);
                }
            }
        }

        #endregion Методы
    }
}
