﻿using System;
using System.Collections.Generic;
using System.Text;
using Astral.Application.Interfaces;
using Astral.Application.Repositories;
using Astral.Domain;
using System.Threading.Tasks;

namespace Astral.Application
{
    public class UnitOfWork : IUnitOfWork
    {
        #region Поля

        /// <summary>
        /// Контекст работы с БД
        /// </summary>
        private AstralContext _db;

        /// <summary>
        /// Репозиторий по вакансиям
        /// </summary>
        private VacancyRepository _vacancyRepository;

        private bool _disposed;

        #endregion Поля

        #region Конструктор

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="db">Контекст работы с БД</param>
        public UnitOfWork(AstralContext db)
        {
            _db = db;
        }

        #endregion Конструктор

        #region Методы

        /// <summary>
        /// Возвращает репозиторий для работы с вакансиями
        /// </summary>
        /// <returns>Репозиторий для работы с вакансиями</returns>
        public IVacancy Vacancy => _vacancyRepository ?? (_vacancyRepository = new VacancyRepository(_db));

        /// <summary>
        /// Сохранение
        /// </summary>
        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }
        
        public virtual void Dispose(bool disposing)
        {
            if (_disposed) return;
            if (disposing)
            {
                _db.Dispose();
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion Методы
    }
}
