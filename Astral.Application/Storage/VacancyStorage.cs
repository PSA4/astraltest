﻿using Astral.Application.Interfaces;
using Astral.Domain;
using Astral.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Astral.Application
{
    public class VacancyStorage : IVacancyStorage
    {
        #region Поля

        /// <summary>
        /// Контекст работы с репозиториями
        /// </summary>
        private readonly IUnitOfWork _unitOfWork;

        #endregion Поля

        #region Конструктор

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="unitOfWork">Контекст работы с репозиториями</param>
        public VacancyStorage(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        #endregion Конструктор

        #region Методы

        /// <summary>
        /// Возвращает список вакансий согласно поисковому слову
        /// </summary>
        /// <param name="searchText">Поисковое слово</param>
        /// <returns>Список вакансий</returns>
        public async Task<List<Vacancy>> GetVacancies(string searchText)
        {
            var result = await _unitOfWork.Vacancy.GetBySearchText(searchText);
            return result;
        }

        /// <summary>
        /// Сохранить вакансию в БД
        /// </summary>
        /// <param name="vacancies">Вакансия(и)</param>
        public async Task SaveVacancy(params Vacancy[] vacancies)
        {
            if (vacancies == null)
                throw new NullReferenceException();

            await DropOldVacancies(vacancies);

            await _unitOfWork.Vacancy.Insert(vacancies);
            await _unitOfWork.Save();
        }

        #endregion Методы

        #region Методы приватные

        /// <summary>
        /// Удаляет старые значения из БД
        /// </summary>
        /// <param name="vacancies">Вакансии</param>
        private async Task DropOldVacancies(params Vacancy[] vacancies)
        {
            await _unitOfWork.Vacancy.Delete(vacancies);
            await _unitOfWork.Save();
        }

        #endregion Методы приватные
    }
}