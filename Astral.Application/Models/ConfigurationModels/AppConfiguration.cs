﻿namespace Astral.Application.Models.ConfigurationModels
{
    /// <summary>
    /// Модель конфигурации приложения
    /// </summary>
    public class AppConfiguration
    {
        /// <summary>
        /// Информация о HH.ru
        /// </summary>
        public HeadHunterInformation HeadHunter { get; set; }
    }
}