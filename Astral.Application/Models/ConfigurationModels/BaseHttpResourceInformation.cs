﻿namespace Astral.Application.Models.ConfigurationModels
{
    /// <summary>
    /// Базовый класс описывающий информацию о стороннем Http ресурсе
    /// </summary>
    public abstract class BaseHttpResourceInformation
    {
        /// <summary>
        /// Ссылка на ресурс
        /// </summary>
        public string EndpointUrl { get; set; }

        /// <summary>
        /// Колличество попыток
        /// </summary>
        public int RetryCount { get; set; }

        /// <summary>
        /// Ключ для авторизации
        /// </summary>
        public string SecreetKey { get; set; }
    }
}