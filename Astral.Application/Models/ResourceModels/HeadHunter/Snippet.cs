﻿using Newtonsoft.Json;

namespace Astral.Application.Models.ResourceModels.HeadHunter
{
    public class Snippet
    {
        [JsonProperty("requirement")]
        public string Requirement { get; set; }

        [JsonProperty("responsibility")]
        public string Responsibility { get; set; }
    }
}