﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Astral.Application.Models.ResourceModels.HeadHunter
{
    public class RootObjectHH
    {
        [JsonProperty("clusters")]
        public object Clusters { get; set; }

        [JsonProperty("items")]
        public List<Item> Items { get; set; } = new List<Item>();

        [JsonProperty("found")]
        public int Found { get; set; }

        [JsonProperty("per_page")]
        public int PerPage { get; set; }
    }
}