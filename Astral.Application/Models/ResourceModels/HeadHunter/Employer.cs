﻿using Newtonsoft.Json;

namespace Astral.Application.Models.ResourceModels.HeadHunter
{
    public class Employer
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("id")]
        public int Id { get; set; }
    }
}