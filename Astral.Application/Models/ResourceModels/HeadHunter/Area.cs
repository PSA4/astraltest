﻿using Newtonsoft.Json;

namespace Astral.Application.Models.ResourceModels.HeadHunter
{
    public class Area
    {
        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}