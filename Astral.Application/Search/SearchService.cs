﻿using Astral.Application.Interfaces;
using Astral.Domain.Exceptions;
using Astral.Domain.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Astral.Application
{
    /// <summary>
    /// Сервис по поиску вакансий
    /// </summary>
    public class SearchService : ISearchService
    {
        #region Поля

        /// <summary>
        /// Интерфейс HTTP-клиента для поиска вакансий из стороннего ресурса
        /// </summary>
        private readonly ISearchHttpClient _searchHttpClient;

        /// <summary>
        /// Интрефейс хранилища вакансий
        /// </summary>
        private readonly IVacancyStorage _vacancyStorage;

        #endregion Поля

        #region Конструктор

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="searchHttpClient">Интерфейс HTTP-клиента для поиска вакансий из стороннего ресурса</param>
        /// <param name="vacancyStorage">Интрефейс хранилища вакансий</param>
        public SearchService(ISearchHttpClient searchHttpClient, IVacancyStorage vacancyStorage)
        {
            _searchHttpClient = searchHttpClient;
            _vacancyStorage = vacancyStorage;
        }

        #endregion Конструктор

        #region Методы

        /// <summary>
        /// Возвращает список вакансий по поиковому слову
        /// </summary>
        /// <param name="text">Поисковое слово</param>
        /// <returns>Список вакансий</returns>
        public async Task<IEnumerable<Vacancy>> GetVacancies(string text)
        {
            var result = default(IEnumerable<Vacancy>);

            try
            {
                var resultFromApi = await _searchHttpClient.GetVacancyAsync(text);

                await _vacancyStorage.SaveVacancy(resultFromApi.ToArray());

                result = resultFromApi;
            }
            catch (VacanciesNotFoundException)
            {
                result = await _vacancyStorage.GetVacancies(text);
            }

            return result;
        }

        #endregion Методы
    }
}