﻿using Astral.Application.Interfaces;
using Astral.Application.Models.ConfigurationModels;
using Astral.Domain.Exceptions;
using Astral.Domain.Models;
using Astral.Application.HttpClients;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Astral.Application
{
    public class SearchHttpClient : ISearchHttpClient
    {
        #region Поля

        /// <summary>
        /// Информация о HH.ru
        /// </summary>
        private readonly HeadHunterInformation _headHunterInformation;
        
        #endregion Поля

        #region Конструкторы

        public SearchHttpClient(IOptions<AppConfiguration> appConfiguration)
        {
            _headHunterInformation = appConfiguration.Value.HeadHunter;
        }

        #endregion Конструкторы

        public async Task<IEnumerable<Vacancy>> GetVacancyAsync(string text)
        {
            var result = default(IEnumerable<Vacancy>);

            try
            {
                result = await new HeadHunterHttpClient(_headHunterInformation).GetVacancyAsync(text);
            }
            catch
            {
                throw new VacanciesNotFoundException("Поиск вакансий не дал результата");
            }

            return result;
        }
    }
}