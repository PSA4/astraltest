﻿using Astral.Application.HttpClentWrappers;
using Astral.Application.Interfaces;
using Astral.Application.Models.ConfigurationModels;
using Astral.Domain.Enums;
using Astral.Domain.Models;
using Astral.Application.Models.ResourceModels.HeadHunter;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Astral.Application.HttpClients
{
    /// <summary>
    /// Http клиент к hh.ru
    /// </summary>
    public class HeadHunterHttpClient : BaseHttpClient, ISearchHttpClient
    {
        #region Конструкторы

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="baseHttpResourceInformation">Информация о ресурсе</param>
        public HeadHunterHttpClient(BaseHttpResourceInformation baseHttpResourceInformation)
            : base(baseHttpResourceInformation.EndpointUrl, baseHttpResourceInformation.RetryCount)
        {
            _httpClient.DefaultRequestHeaders.Accept.Clear();
            _httpClient.DefaultRequestHeaders.Add("User-Agent", baseHttpResourceInformation.SecreetKey);
        }

        #endregion Конструкторы

        #region Методы

        /// <summary>
        /// Возвращает список вакансий с сайта hh.ru
        /// </summary>
        /// <param name="text">Поисковое слово</param>
        /// <returns>Список вакансий</returns>
        public async Task<IEnumerable<Vacancy>> GetVacancyAsync(string text)
        {
            var result = default(IEnumerable<Vacancy>);

            var url = $"/vacancies/?per_page=50&text={text}";

            var responce = await GetAsync<RootObjectHH>(url);

            result = responce?.Items?.Select(i => new Vacancy()
            {
                Name = i.Name,
                Requirement = i.Snippet.Requirement,
                Responsibility = i.Snippet.Responsibility,
                SalaryTo = i.Salary?.To,
                SalaryFrom = i.Salary?.From,
                Currency = i.Salary?.Currency,
                Employer = i.Employer?.Name,
                CreatedDate = i.PublishedAt,
                VacancyId = i.Id,
                ResourceType = ResourceType.HeadHunter
            });

            return result;
        }

        #endregion Методы
    }
}