﻿using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Astral.Application.HttpClentWrappers
{
    /// <summary>
    /// Повторный обработчик сообщений
    /// </summary>
    internal class HttpRetryMessageHandler : DelegatingHandler
    {
        #region Поля

        /// <summary>
        /// Колличество попыток
        /// </summary>
        private readonly int _retryCount;

        #endregion Поля

        #region Конструктор по умолчанию

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        /// <param name="handler">Обработчик сообщений по умолчанию</param>
        /// <param name="retryCount">Количество попыток. По умолчанию одна попытка</param>
        public HttpRetryMessageHandler(HttpClientHandler handler, int retryCount) : base(handler)
        {
            _retryCount = retryCount;
        }

        #endregion Конструктор по умолчанию

        #region Методы

        /// <summary>
        /// Отправка HTTP-запроса в качестве асинхронной операции
        /// </summary>
        /// <param name="request">Сообщение HTTP-запроса для отправки</param>
        /// <param name="cancellationToken">Токен отмены операции</param>
        /// <returns>HTTP ответ</returns>
        protected override async Task<HttpResponseMessage> SendAsync(
            HttpRequestMessage request,
            CancellationToken cancellationToken)
        {
            HttpResponseMessage response = null;
            for (int i = 0; i < _retryCount; i++)
            {
                response = await base.SendAsync(request, cancellationToken);
                if (response.IsSuccessStatusCode)
                {
                    return response;
                }
            }

            return response;
        }

        #endregion Методы
    }
}