﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Astral.Application.HttpClentWrappers
{
    /// <summary>
    /// Http клиент реализующий базовые запросы
    /// </summary>
    public class BaseHttpClient
    {
        #region Поля

        /// <summary>
        /// HTTP клиент для выполнения запросов
        /// </summary>
        public readonly HttpClient _httpClient;

        #endregion Поля

        #region Конструкторы

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="baseAddress">URL адрес ресурса</param>
        /// <param name="version">Версия модели для кэширования</param>
        /// <param name="retryCount">Количество попыток. По умолчанию одна попытка</param>
        public BaseHttpClient(
            string baseAddress,
            int retryCount = 1)
        {
            _httpClient = new HttpClient(new HttpRetryMessageHandler(new HttpClientHandler(), retryCount))
            {
                BaseAddress = new Uri(baseAddress)
            };
        }

        #endregion Конструкторы

        #region Методы

        /// <summary>
        /// Выполнить GET запрос асинхронно
        /// </summary>
        /// <typeparam name="T">Тип возвращаемой модели</typeparam>
        /// <param name="requestUri">Адрес запроса</param>
        public async Task<T> GetAsync<T>(string requestUri)
        {
            using (var response = await _httpClient.GetAsync(requestUri))
            {
                var responseContent = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                return JsonConvert.DeserializeObject<T>(responseContent);
            }
        }

        #endregion Методы

        #region Приватные методы

        /// <summary>
        /// Выполнить асинхронно GET запрос
        /// </summary>
        /// <param name="requestUri">Адрес запроса</param>
        /// <returns>Строку из HTTP ответа</returns>
        internal async Task<string> GetStringAsync(string requestUri)
        {
            using (var response = await _httpClient.GetAsync(requestUri))
            {
                response.EnsureSuccessStatusCode();
                return await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            }
        }

        #endregion Приватные методы
    }
}